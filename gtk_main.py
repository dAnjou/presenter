from gi.repository import Gtk, Gdk, GObject, Pango
from gi.repository import EvinceDocument
from gi.repository import EvinceView

from time import localtime, strftime
import math

PRESENTATION_FILE = "file:///home/max/Dokumente/Studium/Master/fp2/material/themenpraesentation.pdf"

class PresentationWindow(Gtk.Window):
	"""
	This window will show the PDF in fullscreen to the audience.
	"""

	def __init__(self, parent, document_model):
		Gtk.Window.__init__(self, title="Presenter")

		# set a parent for this window
		self.set_transient_for(parent)
		self.set_destroy_with_parent(True)
		self.set_deletable(False)

		# setting up the PDF viewer widget with the same model,
		# that is used for the current slide view in the main window
		slide_view = EvinceView.View()
		slide_view.set_model(document_model)

		# yeah, EvinceView wants to be in a ScrolledWindow -.-
		scroll_window = Gtk.ScrolledWindow()
		scroll_window.add(slide_view)

		# label for a nice overlay
		# when you click on it, the window becomes fullscreen
		label = Gtk.Label()
		label.override_font(Pango.FontDescription("Ubuntu Mono 36"))
		label.set_label("WHAT")

		# make the label clickable ...
		self.eventbox = Gtk.EventBox()
		# ... and transparent
		self.eventbox.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 0.5))
		self.eventbox.connect("button-press-event", self.on_label_clicked)
		self.eventbox.add(label)

		# this widgets makes any other widget an overlay
		overlay = Gtk.Overlay()
		overlay.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 0))
		overlay.add(scroll_window)
		overlay.add_overlay(self.eventbox)
		self.add(overlay)

	def on_label_clicked(self, label, event):
		"""
		callback which makes this window go fullscreen
		"""
		if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 1:
			self.eventbox.hide()
			self.fullscreen()


class MainWindow(Gtk.Window):
	"""
	This window is the "presenter screen".
	"""

	def __init__(self, filename, widgetname):
		Gtk.Window.__init__(self, title="Presenter")

		# load UI from file
		builder = Gtk.Builder()
		builder.add_from_file(filename)

		# get the time label widget and make it a working clock
		time_label = builder.get_object("time")
		def update_time_label():
			time_label.set_text(strftime("%H:%M:%S", localtime()))
			return True
		timer = GObject.timeout_add_seconds(1, update_time_label)

		# yeah, buttons ... obviously
		self.next_button = builder.get_object("next")
		self.previous_button = builder.get_object("previous")
		self.previous_button.set_sensitive(False)

		# get ScrolledWindow widgets for the EvinceViews
		current_slide = builder.get_object("currentslide")
		next_slide = builder.get_object("nextslide")

		# get the PDF and make it an EvinceDocument
		EvinceDocument.init()
		self.doc = EvinceDocument.Document.factory_get_document(PRESENTATION_FILE)

		# okay, need 2 models because models represent the state of the
		# current view. only one model would not work because i have to
		# display the current and the next slide, you can't do that with one
		# and the same model

		# should be quite obvious
		self.current_model = EvinceView.DocumentModel()
		self.current_model.set_document(self.doc)
		self.current_model.set_page(0)
		self.current_model.set_sizing_mode(EvinceView.SizingMode.BEST_FIT)
		self.current_model.set_continuous(True)
		current_view = EvinceView.View()
		current_view.set_model(self.current_model)
		current_slide.add(current_view)

		# create a PresentationWindow that uses the same model as the current
		# slide view. remember that all views with this model will be in the
		# same state. so when i go through the pages, current_view and the
		# view in the PresentationWindow will show the same thing .. cool, huh? :P
		self.p = PresentationWindow(self, self.current_model)

		self.next_model = EvinceView.DocumentModel()
		self.next_model.set_document(self.doc)
		self.next_model.set_page(self.current_model.get_page() + 1)
		self.next_model.set_sizing_mode(EvinceView.SizingMode.BEST_FIT)
		self.next_model.set_continuous(False)
		self.next_model.set_dual_page(True)
		next_view = EvinceView.View()
		next_view.set_model(self.next_model)
		next_slide.add(next_view)

		# what happens when i click a button?
		handlers = {
			"on_start_toggled": self.on_start_toggled,
			"on_next_clicked": self.on_next_clicked,
			"on_previous_clicked": self.on_previous_clicked
		}
		builder.connect_signals(handlers)
		self.add(builder.get_object(widgetname))

	def on_start_toggled(self, button):
		if button.get_active():
			button.set_label("Stop")
			self.p.show_all()
		else:
			button.set_label("Start")
			self.p.unfullscreen()
			self.p.hide()

	def on_next_clicked(self, button):
		self.current_model.set_page(self.current_model.get_page() + 1)

	def on_previous_clicked(self, button):
		self.current_model.set_page(self.current_model.get_page() - 1)

	def on_current_page_changed(self, document_model, previous_page, current_page):
		# browsing the pages here
		# and deactivating buttons if there are no next or previous pages
		if current_page == self.doc.get_n_pages() - 1:
			self.next_button.set_sensitive(False)
		elif current_page == 0:
			self.previous_button.set_sensitive(False)
		else:
			self.previous_button.set_sensitive(True)
			self.next_button.set_sensitive(True)
		if current_page < self.doc.get_n_pages() - 1:
			self.next_model.set_page(current_page + 1)


if __name__ == '__main__':
	# create a window
	window = MainWindow("gtk_main.ui", "main_box")
	# quit on close signal
	window.connect("delete-event", Gtk.main_quit)
	window.show_all()

	Gtk.main()